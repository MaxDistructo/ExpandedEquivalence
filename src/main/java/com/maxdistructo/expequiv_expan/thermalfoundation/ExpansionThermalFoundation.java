package com.maxdistructo.expequiv_expan.thermalfoundation

import java.util.List;

import com.zeitheron.expequiv.exp.Expansion;
import com.zeitheron.expequiv.exp.ExpansionReg;
import com.zeitheron.expequiv.exp.reborncore.RebornCoreEMCMapper;

import moze_intel.projecte.api.proxy.IEMCProxy;
import moze_intel.projecte.emc.json.NormalizedSimpleStack;
import moze_intel.projecte.emc.mappers.IEMCMapper;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

/* @class ExpansionThermalFoundation
* Adds a few materials to the EMC list from thermal foundation.
*/

@ExpansionReg(modid = "thermalfoundation")
public class ExpansionThermalFoundation extends Expansion{

    public ExpansionThermalFoundation(String modid, Configuration config, Object[] args)
	{
		super(modid, config, args);
	}

    @Override
	protected void addCfgEMC()
	{
		addEMCCfg(4096, "EnderiumIngot");
		addEMCCfg(2048, "LumiumIngot");
		addEMCCfg(2048, "SignaliumIngot");
		addEMCCfg(2048, "ConstantanIngot");
		//addEMCCfg(int EMCValue, String ItemName);
	}
	
	@Override
	public void registerEMC(IEMCProxy emcProxy)
	{
		Item item_material = ForgeRegistries.ITEMS.getValue(new ResourceLocation("thermalfoundation", "item_material"))
		addEMC(item_material, 164, "ConstantanIngot");
		addEMC(item_material, 165, "SignaliumIngot");
		addEMC(item_material, 166, "LumiumIngot");
		addEMC(item_material, 167, "EnderiumIngot");
        //addEMC(Item, int meta, String name)
		//Item item_material = ForgeRegistries.ITEMS.getValue(new ResourceLocation("enderio", "item_material"));
	}
	
	@Override
	public void getMappers(List<IEMCMapper<NormalizedSimpleStack, Integer>> mappers)
	{
		//mappers.add(new AlloySmelterEMCMapper()); In case I want/need to do it for the machine recipes.
		//mappers.add(new SAGEMCMapper());
		//mappers.add(new SliceNSpliceEMCMapper());
		//mappers.add(new VatEMCMapper());
		
		mappers.add(new RebornCoreEMCMapper());
	}

}