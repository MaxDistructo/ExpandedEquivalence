package com.maxdistructo.expequiv_expan.thermalfoundation

import com.zeitheron.expequiv.exp.CraftingIngredients;
import moze_intel.projecte.emc.IngredientMap;
import moze_intel.projecte.emc.collector.IMappingCollector;
import moze_intel.projecte.emc.json.NSSItem;
import moze_intel.projecte.emc.json.NormalizedSimpleStack;
import moze_intel.projecte.emc.mappers.IEMCMapper;
import net.minecraftforge.common.config.Configuration;

public class PulverizerEMCMapper{
    @Override
	public void addMappings(IMappingCollector<NormalizedSimpleStack, Integer> mapper, Configuration cfg)
	{
        //TODO Finish this.
	}
	
	@Override
	public String getName()
	{
		return "TF_Pulverizer_Mappter";
	}
	
	@Override
	public String getDescription()
	{
		return "Adds EMC to Pulverizer Recipes";
	}
	
	@Override
	public boolean isAvailable()
	{
		return true;
	}
}